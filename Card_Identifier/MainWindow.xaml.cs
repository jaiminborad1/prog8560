﻿//Code By: Aniket Rana
using System.Windows;

namespace Card_Identifier
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void BtnImage1_Click(object sender, RoutedEventArgs e)
        {
            MessageLabel.Content = "King Diamonds";
        }
        private void BtnImage2_Click(object sender, RoutedEventArgs e)
        {
            MessageLabel.Content = "3 Clubs";
        }
        private void BtnImage3_Click(object sender, RoutedEventArgs e)
        {
            MessageLabel.Content = "Queen Hearts";
        }
        private void BtnImage4_Click(object sender, RoutedEventArgs e)
        {
            MessageLabel.Content = "5 Spades";
        }
        private void BtnImage5_Click(object sender, RoutedEventArgs e)
        {
            MessageLabel.Content = "Ace Hearts";
        }
        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            MessageLabel.Content = "";
        }
    }
}
